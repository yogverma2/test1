//
//  test1App.swift
//  test1
//
//  Created by Yogesh Kumar Verma on 27/08/21.
//

import SwiftUI

@main
struct test1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
