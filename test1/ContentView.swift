//
//  ContentView.swift
//  test1
//
//  Created by Yogesh Kumar Verma on 27/08/21.
//

import SwiftUI

struct ContentView: View {
    @State var total = "20"
    @State var tipPercent: Double = 15.0
    
    var body: some View {
        VStack{
            Text("Tip Calculator")
                .font(.title)
                .bold()
                
            HStack {
                Text("$")
                TextField("Total", text: $total)
                    .border(Color.black, width: 0.25)
            }
            .padding()
            
            HStack {
                Slider(value: $tipPercent, in: 1...40, step: 1.0)
                Text("\(Int(tipPercent))%")
            }
            .padding()
            
            if let totalNum = Double(total){
                Text("Tip Amount: $\(totalNum * tipPercent / 100, specifier: "%0.2f")")
            } else {
                Text("Please Enter a Numeric Value.")
            }
            
        }
//        Text("Hello, world! to Xcode12...")
//            .padding()
//        font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
